Play with django and async mode
===========================

nginx + uwsgi (processes/threads) + django: `$ docker-compose -f docker-compose.sync.yml up`



nginx + uwsgi (processes/gevent) + django (+psycopgreen): `$ docker-compose -f docker-compose.async.yml up`



Tests
-----

Run in parallel:

`$ ab -n 10000 -c 50 'http://localhost:8080/api/slow'`

`$ ab -n 10000 -c 50 'http://localhost:8080/api/fast'`

or in parallel:

`$ wrk -t50 -c50 -d10s http://localhost:8080/api/slow'`

`$ wrk -t50 -c50 -d10s http://localhost:8080/api/fast'`
