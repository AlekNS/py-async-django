import time
from django.http import JsonResponse

from django.db import connection

def slow_response(request):
    msg = ''
    with connection.cursor() as c:
        c.execute("select pg_sleep(0.75), 'Hello slow world.'")
        _, msg = c.fetchone()
    data = {'message': msg}
    return JsonResponse(data)

def fast_response(request):
    data = {'message': 'Hello fast world.'}
    return JsonResponse(data)
